$(document).ready( function() {

    var afterMove = function (index) {
        if (index == 1) {
            $('#prev-slide').css({'opacity': '0', 'height': '0px'});
            $('#nav-menu--main').addClass('active');
        } else {
            $('#prev-slide').css({'opacity': '1', 'height': '80px'});
            $('#nav-menu--main').removeClass('active');
        }
        if (index == 2) {
            $('#nav-menu--about').addClass('active');
        } else {
            $('#nav-menu--about').removeClass('active');
        }
        if (index == 3) {
            $('#nav-menu--projects').addClass('active');
        } else {
            $('#nav-menu--projects').removeClass('active');
        }
        if(index == 4) {
            $('#next-slide').css({'opacity': '0', 'height': '0px'});
            $('#first-slide').css({'opacity': '1', 'height': '60px'});
            $('#nav-menu--contacts').addClass('active');
        } else {
            $('#next-slide').css({'opacity': '1', 'height': '60px'});
            $('#first-slide').css({'opacity': '0', 'height': '0px'});
            $('#nav-menu--contacts').removeClass('active');
        }

    };

    $('.main-wrap').onepage_scroll({
        sectionContainer: "section",     // sectionContainer accepts any kind of selector in case you don't want to use section
        easing: "ease",                  // Easing options accepts the CSS3 easing animation such "ease", "linear", "ease-in",
                                         // "ease-out", "ease-in-out", or even cubic bezier value such as "cubic-bezier(0.175, 0.885, 0.420, 1.310)"
        animationTime: 500,             // AnimationTime let you define how long each section takes to animate
        pagination: false,                // You can either show or hide the pagination. Toggle true for show, false for hide.
        updateURL: false,                // Toggle this true if you want the URL to be updated automatically when the user scroll to each page.
        beforeMove: function(index) {},  // This option accepts a callback function. The function will be called before the page moves.
        afterMove: afterMove,   // This option accepts a callback function. The function will be called after the page moves.
        loop: false,                     // You can have the page loop back to the top/bottom when the user navigates at up/down on the first/last page.
        keyboard: true,                  // You can activate the keyboard controls
        responsiveFallback: false,        // You can fallback to normal page scroll by defining the width of the browser in which
        // you want the responsive fallback to be triggered. For example, set this to 600 and whenever
        // the browser's width is less than 600, the fallback will kick in.
        direction: "vertical"            // You can now define the direction of the One Page Scroll animation. Options available are "vertical" and "horizontal". The default value is "vertical".
    });

    $('.carousel-wrap').css('height', $(window).height()-180);
    $('.carousel-wrap img').css('height', $('.carousel-wrap').height()/3);
    //$('.carousel-wrap img').css('width', $('.carousel-wrap').width()/2);

    $(window).resize(function(){
        $('.carousel-wrap').css('height', $(window).height()-180);
        $('.carousel-wrap img').css('height', $('.carousel-wrap').height()/3);
        //$('.carousel-wrap img').css('width', $('.carousel-wrap').width()/2);
    });

    $('#next-slide').click(function(){
        $('.main-wrap').moveDown();
    });

    $('#prev-slide').click(function(){
        $('.main-wrap').moveUp();
    });

    $('#first-slide').click(function(){
        $('.main-wrap').moveTo(1);
    });

    $('#nav-menu--main').click(function() {
        $('.main-wrap').moveTo(1);
    });

    $('#nav-menu--about').click(function() {
        $('.main-wrap').moveTo(2);
    });

    $('#nav-menu--projects').click(function() {
        $('.main-wrap').moveTo(3);
    });

    $('#nav-menu--contacts').click(function() {
        $('.main-wrap').moveTo(4);
    });
});