var gulp = require("gulp"),
    connect = require("gulp-connect"),
    opn = require("opn"),
    csso = require('gulp-csso'),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    jade = require('gulp-jade'),
    stylus = require("gulp-stylus"),
    coffee = require('gulp-coffee'),
    concat = require('gulp-concat');

//***********************************//
// 1.Локальный сервер и автообновление //
//***********************************//

// Запуск локального сервера
gulp.task('connect', function(){
    connect.server({
        root: 'app',
        livereload: true,
        port: 8888
    });
    opn('http://localhost:8888');
});

// Слежение за HTML файлами
gulp.task('html', function() {
    gulp.src('./app/index.html')
        .pipe(connect.reload());
});

// Слежение за CSS файлами
gulp.task('css', function() {
    gulp.src('./app/css/app.css')
        .pipe(plumber())
        .pipe(connect.reload());
});

// Слежение за JS файлами
gulp.task('js', function() {
    gulp.src('./app/js/app.js')
        .pipe(connect.reload());
});

// Компиляция Stylus
gulp.task('stylus', function (){
    return gulp.src('./app/dev/stylus/app.styl')
        .pipe(plumber())
        .pipe(stylus({
            compress: true
        }))
        .pipe(autoprefixer(
            'last 7 version',
            '> 1%',
            'ie 8',
            'ie 9',
            'ios 6',
            'android 4'
        ))
        .pipe(csso())
        .pipe(gulp.dest('./app/css'));
});

// Компиляция Jade для index.jade
gulp.task('jade-index', function(){
    gulp.src("./app/dev/jade/index.jade")
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest("./app/"));
});

// Запуск слежения
gulp.task('watch', function() {
   gulp.watch(['./app/*.html'], ['html']);
   gulp.watch(['./app/css/*.css'], ['css']);
   gulp.watch(['./app/js/*.js'], ['js']);
   gulp.watch(['./app/dev/jade/index.jade'], ['jade-index']);
   gulp.watch(['./app/dev/stylus/*.styl'], ['stylus']);
});

//***********************************//
// /1.Локальный сервер и автообновление//
//***********************************//

// Зачада по умолчанию
gulp.task('default',['connect', 'watch']);